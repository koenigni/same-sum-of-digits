#pragma once

#include "work_queue.h.cu"

struct RunConfig: DefaultConfig {
  static constexpr int NThreads = 256;
  static constexpr int NBlocks = 4 * 46;

  static constexpr int IBSize = 60;
  static constexpr bool IssueInitial = true;
  static constexpr int NumExtraBlocks = 1 << 24;
  static constexpr bool UseSimpleBlockScheme = true;

  struct SimpleBlockScheme {
	static constexpr int BSize = IBSize;
  };

  struct FancyBlockScheme {
	// __device__ __host__ static constexpr float packagebits(int s) {
	//   return 0.55 * (s - IBSize) + IBSize - 32;
	// }
	FUNC_SOURCE(__device__ __host__ static constexpr, packagebits,
				return 0.55f * (s - IBSize) + IBSize - 28, int s)  };

  static constexpr int SuccessQueueSize = 28;
  static constexpr int AsyncRBSize = 28;
  static constexpr int WThreadSize = 0x400;

  static constexpr int MinCopyDuration = 200;

  static constexpr int InitialPackageSize = 28;

  static constexpr int EmergencyPkgSizeDecrease = 1;
  struct PID {
	static constexpr float P = 10;
	static constexpr float I = 0.015;
	static constexpr float D = 100;
	static constexpr float Ddec = 0.8;
  };

  static constexpr bool show_gui = false;

  static constexpr char result_fname[] = "results.dat";
};

