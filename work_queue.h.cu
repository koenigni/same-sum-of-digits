#pragma once

#include "util.h.cu"

#include <cooperative_groups.h>
#include <cooperative_groups/memcpy_async.h>

#include <assert.h>
#include <chrono>
#include <thread>

namespace cg = cooperative_groups;

namespace Device {
  __attribute__((pure)) __device__ static inline unsigned int lanemask_lt() {
	unsigned ret;
	asm("mov.u32 %0, %%lanemask_lt;" : "=r"(ret));
	return ret;
  }

  __attribute__((pure)) __device__ static inline int warp_id() {
	return threadIdx.x >> 5;
  }

  using warp = cg::thread_block_tile<32, cg::thread_block>;

  __attribute__((pure)) __device__ static inline warp get_warp() {
	return cg::tiled_partition<32>(cg::this_thread_block());
  }

  __attribute__((pure)) __device__ static inline int wthread_rank() {
	return threadIdx.x & 0x1f;
  };

  __attribute__((pure)) __device__ static inline int tid() {
	return threadIdx.x + blockDim.x * blockIdx.x;
  }

  template<typename F, typename... Args>
  __device__ auto invoke_one_broadcast_warp(F f, Args... args) {
	decltype(f(args...)) r;
	if (wthread_rank() == 0)
	  r = f(args...);
	return get_warp().shfl(r, 0);
  }

  static constexpr int CacheLineSize = 128;
  static constexpr int PageSize = 4096;

  static constexpr int WarpSize = 32;

  __device__ static inline void sync_active() {
	__syncwarp(__activemask());
  }

  template<typename T, int S>
  union Padded {
	T v;
	char padding[S];

	__host__ __device__ operator T() { return v; }
	__host__ __device__ T &operator()() { return v; }
  };

  template<typename T>
  using CacheLine = Padded<T, CacheLineSize>;

  template<typename T>
  using Page = Padded<T, PageSize>;
};// namespace Device


struct DefaultConfig {
  static constexpr int NThreads = 256;
  static constexpr int NBlocks = 1024;
  static constexpr int QueueSize = 2048;

  static constexpr int SuccessQueueSize = 25;
  static constexpr int AsyncSafetyOffset = 1 << 16;

  static constexpr int MinPackageSize = 8;

  static constexpr int NumExtraBlocks = 0;

  static constexpr int CritialCalcDuration = 250;
};

namespace Detail {
  namespace D = Device;

  static inline __device__ int mod(int a, int b) {
	return (a + b) % b;
  }

  template<typename T>
  struct Optional {
	T t;

	__device__ Optional(T t, bool b): t(b ? t : T::invalid()) {}
	__device__ Optional(): t(T::invalid()) {}
	__device__ Optional(T t): t(t) {}
	Optional(const Optional &) = default;

	__device__ inline bool valid() { return !t.is_invalid(); };
	__device__ inline operator bool() { return valid(); }
	__device__ inline operator T() { return t; }
  };


  template<typename T, typename Config>
  struct Distributor {
	static inline __device__ int *head() {
	  static __shared__ int head[Config::NThreads / D::WarpSize];
	  return &head[Device::warp_id()];
	}

	struct HSInterface {
	  T *t;
	  __host__ HSInterface() {
		CUDA_CHECK(cudaMalloc(&t, Config::QueueSize * Config::NThreads
										  / D::WarpSize * Config::NBlocks
										  * sizeof(T)));
	  }

	  __host__ void free() { CUDA_CHECK(cudaFree(t)); }
	};

	__device__ static Distributor from(HSInterface a) {
	  T *t = &a.t[blockIdx.x * Config::NThreads / D::WarpSize
						  * Config::QueueSize
				  + D::warp_id() * Config::QueueSize];
	  return {t};
	}

	T *ptr;

	__device__ void push_index(D::warp w, T t, int i, int total) {
	  // DPRINT("%2d.2 ->   .p (%d/%d)", w.thread_rank(), i, total);
	  int h = *head();
	  D::sync_active();
	  assert(h + i < Config::QueueSize);
	  ptr[h + i] = t;
	  if (i == 0)
		*head() = h + total;
	}
	__device__ bool pop_index(D::warp w, T &t, int i, int total) {
	  int h = *head();
	  int index = h - i - 1;
	  if (index >= 0) {
		// DPRINT("  .p -> %2d.1 (%d/%d)", w.thread_rank(), i, total);
		t = ptr[index];
	  }
	  if (i == 0)
		*head() = max(h - total, 0);
	  return index >= 0;
	}

	__device__ Optional<T> reshuffle(D::warp w, bool valid, T t1, T t2) {
	  int mask = w.ballot(valid);

	  int na = __popc(mask);

	  int f1 = __fns(mask, 0, D::wthread_rank() + 1);
	  int f2 = __fns(mask, 0, mod(D::wthread_rank() - na, D::WarpSize) + 1);

	  // DPRINT("%2d | %2d", f1, f2);

	  t1 = w.shfl(t1, f1);
	  t2 = w.shfl(t2, f2);

	  if (f2 >= 0 && f1 < 0)
		t1 = t2;

	  bool ret = f1 >= 0 || f2 >= 0;

	  if (f1 < 0 && f2 < 0) {
		ret = pop_index(w, t1, D::WarpSize - D::wthread_rank() - 1,
						D::WarpSize - 2 * na);
	  } else if (f1 >= 0 && f2 >= 0)
		push_index(w, t2, D::wthread_rank(), 2 * na - D::WarpSize);

	  w.sync();

	  return Optional<T>{t1, ret};
	}
  };

  template<typename T, typename Config>
  struct CPUXLink {
	static constexpr int SQueueSize = 1 << Config::SuccessQueueSize;

	struct XLink {
	  D::CacheLine<int> acc;
	  D::CacheLine<int> squeue_head;
	  T t[SQueueSize];
	};

	static inline __device__ XLink **xlink_mem() {
	  static __shared__ XLink *xlink;
	  return &xlink;
	}

	static inline __device__ XLink *xlink() { return *xlink_mem(); }

	struct HSInterface {
	  XLink *q;

	  __host__ HSInterface() {
		CUDA_CHECK(cudaMalloc(&q, sizeof(XLink)));
		XLink *initxl = new XLink;
		initxl->acc() = 0;
		initxl->squeue_head() = 0;
		cudaMemcpy(q, initxl, sizeof(XLink), cudaMemcpyHostToDevice);
		cudaDeviceSynchronize();
		delete initxl;
	  }

	  __host__ void free() { CUDA_CHECK(cudaFree(q)); }

	private:
	  template<typename F, typename... Args>
	  __host__ int execute_step(XLink *hostlocal, int last_num, bool last, F &f,
								Args... args) {
		struct Indexer {
		  XLink *hostlocal;
		  int off;
		  int n;

		  __host__ auto operator[](int i) {
			return hostlocal->t[(off + i) & (SQueueSize - 1)];
		  }

		  __host__ int num() { return n; }

		  __host__ int num_issued() { return hostlocal->acc; }
		};

		int n;
		unsigned int safety_offset = last ? 0 : Config::AsyncSafetyOffset;
		if (((hostlocal->squeue_head - last_num) & (SQueueSize - 1))
			>= safety_offset)
		  n = (hostlocal->squeue_head - last_num - safety_offset)
			  & (SQueueSize - 1);
		else
		  n = 0;

		f(Indexer{hostlocal, last_num, n}, args...);

		return (last_num + n) & (SQueueSize - 1);
	  }

	public:
	  template<typename F, typename... Args>
	  __host__ void execute_async(cudaEvent_t calc_stopped, F &f,
								  Args... args) {
		XLink *hostlocal[2];
		cudaHostAlloc(&hostlocal[0], sizeof(XLink), 0);
		cudaHostAlloc(&hostlocal[1], sizeof(XLink), 0);

		int last_num = 0;
		int currhl = 0;
		cudaStream_t s;
		cudaStreamCreate(&s);

		cudaMemcpyAsync(hostlocal[currhl], q, sizeof(XLink),
						cudaMemcpyDeviceToHost, s);
		f.start_calc();
		while (cudaEventQuery(calc_stopped) != cudaSuccess) {
		  cudaStreamSynchronize(s);
		  int nexthl = currhl ^ 1;
		  cudaMemcpyAsync(hostlocal[nexthl], q, sizeof(XLink),
						  cudaMemcpyDeviceToHost, s);
		  last_num =
				  execute_step(hostlocal[currhl], last_num, false, f, args...);
		  currhl = nexthl;
		}
		cudaStreamSynchronize(s);
		last_num = execute_step(hostlocal[currhl], last_num, false, f, args...);
		currhl = currhl ^ 1;
		cudaMemcpyAsync(hostlocal[currhl], q, sizeof(XLink),
						cudaMemcpyDeviceToHost, s);
		cudaStreamSynchronize(s);
		execute_step(hostlocal[currhl], last_num, true, f, args...);

		cudaStreamDestroy(s);
		CUDA_CHECK(cudaFreeHost(hostlocal[0]));
		CUDA_CHECK(cudaFreeHost(hostlocal[1]));
	  }
	};

	__device__ static CPUXLink from(HSInterface a) {
	  *xlink_mem() = a.q;
	  return {};
	}

	__device__ void push(T t) {
	  auto ct = cg::coalesced_threads();
	  unsigned int index =
			  invoke_one_broadcast(ct,
								   [=]() {
									 return atomicAdd(&xlink()->squeue_head(),
													  ct.num_threads());
								   })
			  + ct.thread_rank();
	  assert(sizeof(T) == sizeof(int4));

	  __stwb((int4 *) &xlink()->t[index & (SQueueSize - 1)], convert<int4>(t));
	}


	static constexpr long NOT_FOUND = 0;

	__device__ long fetch_extra(D::warp w) {
	  int *p = &xlink()->acc();
	  if (*p >= Config::NumExtraBlocks)
		return NOT_FOUND;
	  auto local =
			  D::invoke_one_broadcast_warp([=]() { return atomicAdd(p, 1); });
	  return 32l * local + D::wthread_rank()
			 + Config::NBlocks * Config::NThreads;
	}
  };

  template<typename T, typename Runner, typename Config>
  struct Loop {
	Distributor<T, Config> dist;
	Runner runner;
	CPUXLink<T, Config> success_stack;

	using DHSI = typename Distributor<T, Config>::HSInterface;
	using RHSI = typename Runner::HSInterface;
	using XLHSI = typename CPUXLink<T, Config>::HSInterface;

	struct HSInterface: DHSI, RHSI, XLHSI {
	  __host__ HSInterface(): DHSI(), RHSI(), XLHSI() {}

	  __host__ void free() {
		static_cast<DHSI *>(this)->free();
		static_cast<RHSI *>(this)->free();
		static_cast<XLHSI *>(this)->free();
	  }
	};

	__device__ static Loop from(HSInterface ads) {
	  return {Distributor<T, Config>::from(ads), Runner::from(ads),
			  CPUXLink<T, Config>::from(ads)};
	}

	struct Interface {
	  struct PairT {
		T a, b;

		__device__ static constexpr PairT invalid() {
		  return {T::invalid(), T::invalid()};
		}
		__device__ bool is_invalid() {
		  assert(a.is_invalid() == b.is_invalid());
		  return a.is_invalid();
		}
	  };
	  using Result = Optional<PairT>;

	  CPUXLink<T, Config> s;

	  __device__ Result ret() { return {}; }
	  __device__ Result ret(T a, T b) { return {PairT{a, b}}; };
	  __device__ Result success(T t) {
		// DPRINT("SUCCESS: %#ld-%#ld", (uint64_t) t.lv(), (uint64_t) t.hv());
		s.push(t);
		return ret();
	  }
	};

	__device__ void loop(D::warp w) {
	  long workpackage_id = D::tid();
	  do {
		Optional<T> ot{runner.initial(workpackage_id)};
		while (w.ballot(ot)) {
		  typename Interface::Result res{};
		  if (ot)
			res = runner.run(Interface{success_stack}, ot);
		  w.sync();
		  ot = dist.reshuffle(w, res.valid(), res.t.a, res.t.b);
		}
		workpackage_id = success_stack.fetch_extra(w);
	  } while (workpackage_id != CPUXLink<T, Config>::NOT_FOUND);
	}
  };

  template<typename T, typename Runner, typename Config>
  __global__ __launch_bounds__(Config::NThreads) void entry_point(
		  typename Loop<T, Runner, Config>::HSInterface a) {
	auto l = Loop<T, Runner, Config>::from(a);
	cg::this_thread_block().sync();
	auto w = D::get_warp();
	l.loop(w);
  }
};// namespace Detail

namespace WQInterface {
  template<typename T, typename Runner, typename Config>
  struct Task {
	template<typename F, typename... Args>
	static __host__ auto run(F &f, Args... fargs) {
	  using Loop = Detail::Loop<T, Runner, Config>;

	  cudaDeviceSetCacheConfig(cudaFuncCachePreferL1);
	  cudaEvent_t kernel_started, kernel_finished;
	  cudaStream_t kernel_stream;
	  cudaStreamCreate(&kernel_stream);
	  cudaEventCreate(&kernel_started);
	  cudaEventCreate(&kernel_finished);

	  typename Loop::HSInterface interface {};

	  cudaEventRecord(kernel_started, kernel_stream);
	  Detail::entry_point<T, Runner, Config>
			  <<<Config::NBlocks, Config::NThreads, 0, kernel_stream>>>(
					  interface);

	  cudaEventRecord(kernel_finished, kernel_stream);
	  interface.execute_async(kernel_finished, f, interface, fargs...);
	  cudaStreamSynchronize(kernel_stream);
	  float t;
	  cudaEventElapsedTime(&t, kernel_started, kernel_finished);


	  cudaEventDestroy(kernel_finished);
	  cudaStreamDestroy(kernel_stream);

	  interface.free();

	  return t;
	}
  };
}// namespace WQInterface

