#define BIGREM CONCAT(bigrem_, BASE)
#define BIGDIV CONCAT(bigdiv_, BASE)
#define CALC   CONCAT(calc_, BASE)

/* Varianten von bigrem.  */

__device__ __host__ static inline unsigned int
BIGREM(__uint128_t a) {
  unsigned int x3, x2, x1, x0;
  const uint64_t m1 = MUL64(32);
  const uint64_t m2 = MUL64(64);
  const uint64_t m3 = MUL64(96);
  uint64_t tmp;
  x0 = a;
  x1 = a >> 32;
  x2 = a >> 64;
  x3 = a >> 96;
  tmp = x0 + x1 * m1 + x2 * m2 + x3 * m3;
  return tmp % N_BLOCK;
}

#define MAGIC (MAGIC_HI * TWO_64 + MAGIC_LOW)

__device__ __host__ static inline __uint128_t
BIGDIV(__uint128_t a) {
  const __uint128_t magic = MAGIC;
  return a * magic;
}

#undef MAGIC

__device__ __host__ static inline void
CALC(__uint128_t from, unsigned int range_bit, unsigned int *lowval,
	 unsigned int *highval) {
  __uint128_t to = from + (ONE << range_bit) - 1;
  uint64_t to_64, from_64;
  bool lower_zeros = true;
  bool upper_nines = true;
  unsigned int p_from, p_to;
  uint16_t tentative_diff = 0;
  uint16_t assured_diff = 0;
  uint16_t current_lower = 0;
  uint16_t current_upper = 0;
  short int ai, bi;

#if DEBUG
  printf("calc_%d:", BASE);
#endif
#pragma unroll(1)
  for (int i = 0; i < N1; i++) {
	p_from = BIGREM(from);
	from = BIGDIV(from - p_from);
	p_to = BIGREM(to);
	to = BIGDIV(to - p_to);
	for (int j = 0; j < BLOCKSZ; j++) {
	  ai = p_from % BASE;
	  p_from = p_from / BASE;
	  bi = p_to % BASE;
	  p_to = p_to / BASE;
#if DEBUG
	  printf("%u ", bi);
#endif
	  if (bi <= ai) {
		tentative_diff += BASE - 1;
		current_lower += ai;
		current_upper += bi;
	  } else if (ai < bi) {
		assured_diff += tentative_diff;
		current_lower = ai + (lower_zeros ? 0 : 1);
		current_upper = assured_diff + bi - (upper_nines ? 0 : 1);
		tentative_diff = BASE - 1;
	  }
	  lower_zeros &= ai == 0;
	  upper_nines &= bi == BASE - 1;
	}
  }
  from_64 = from;
  to_64 = to;
#pragma unroll(1)
  for (int i = N1; i < N2 - 1; i++) {
	p_from = from_64 % N_BLOCK;
	from_64 = from_64 / N_BLOCK;
	p_to = to_64 % N_BLOCK;
	to_64 = to_64 / N_BLOCK;
	for (int j = 0; j < BLOCKSZ; j++) {
	  ai = p_from % BASE;
	  p_from = p_from / BASE;
	  bi = p_to % BASE;
	  p_to = p_to / BASE;
#if DEBUG
	  printf("%u ", bi);
#endif
	  if (bi <= ai) {
		tentative_diff += BASE - 1;
		current_lower += ai;
		current_upper += bi;
	  } else if (ai < bi) {
		assured_diff += tentative_diff;
		current_lower = ai + (lower_zeros ? 0 : 1);
		current_upper = assured_diff + bi - (upper_nines ? 0 : 1);
		tentative_diff = BASE - 1;
	  }
	  lower_zeros &= ai == 0;
	  upper_nines &= bi == BASE - 1;
	}
  }
#pragma unroll(1)
  for (int j = 0; j < BLOCK_REST; j++) {
	ai = p_from % BASE;
	p_from = p_from / BASE;
	bi = p_to % BASE;
	p_to = p_to / BASE;
#if DEBUG
	printf("%u ", bi);
#endif
	if (bi <= ai) {
	  tentative_diff += BASE - 1;
	  current_lower += ai;
	  current_upper += bi;
	} else if (ai < bi) {
	  assured_diff += tentative_diff;
	  current_lower = ai + (lower_zeros ? 0 : 1);
	  current_upper = assured_diff + bi - (upper_nines ? 0 : 1);
	  tentative_diff = BASE - 1;
	}
	lower_zeros &= ai == 0;
	upper_nines &= bi == BASE - 1;
  }
  *lowval = current_lower;
  *highval = current_upper;
#if DEBUG
  printf("\n");
#endif
}
