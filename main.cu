// #define DEBUG_MODE
#define NDEBUG

#include "work_queue.h.cu"
// #include "x-poc.h.cu"
#include "conf.h.cu"
#include "fancy_window/fancy_window.hpp"
#include "hilo.h.cu"

#include <algorithm>
#include <chrono>
#include <omp.h>
#include <stdio.h>
#include <string_view>

using namespace std::chrono_literals;

template<typename T, typename Config>
struct Package {
  T v;

  __device__ __host__ static inline Package create(T v, int bsize) {
	return {(v & ~0xffl) | (bsize & 0xffl)};
  }

  __device__ __host__ unsigned bsize() { return v & T{0xff}; }
  __device__ __host__ T value() { return v & ~T{0xffl}; }

  __device__ __host__ Package split_lower() {
	return create(value(), bsize() - 1);
  }
  __device__ __host__ Package split_upper() {
	return create(value() | (T{1} << (bsize() - 1)), bsize() - 1);
  }

  __device__ __host__ T lv() { return value(); };
  __device__ __host__ T hv() { return value() + (T{1} << bsize()) - 1; }

  template<typename Runner>
  __device__ bool splittable(Runner r) {
	return bsize() > r.min_package_size();
  }

  __device__ static inline Package invalid() { return {0}; }
  __device__ bool is_invalid() { return v == 0; }
};


template<typename Config>
struct Runner {
  using Int = __uint128_t;
  using Package = Package<Int, Config>;

  union PSCtrlr {
	struct IFace {
	  volatile int num;
	  volatile int frac;
	} i;
	char padding[Device::PageSize];
  };

  static inline __device__ auto **getptrmem() {
	static __shared__ typename PSCtrlr::IFace *p;
	return &p;
  }

  static inline __device__ auto getiface() { return **getptrmem(); }

  struct HSInterface {
	PSCtrlr *p;

	__host__ HSInterface() {
	  CUDA_CHECK(cudaMallocManaged(&p, sizeof(PSCtrlr)));
	  p->i.num = Config::InitialPackageSize;
	  p->i.frac = 0;
	};

	__host__ void adjust_pkgsize(int new_val, int frac) {
	  p->i = typename PSCtrlr::IFace{new_val, frac};
	}

	__host__ void free() { CUDA_CHECK(cudaFree(p)); }
  };


  __device__ static Runner from(HSInterface h) {
	*getptrmem() = &h.p->i;
	return {};
  }

  __device__ int min_package_size() {
	auto iface = getiface();
	return blockIdx.x < iface.frac ? iface.num : iface.num + 1;
  }

  __host__ __device__ static auto initial_pair_fancy(long i) {
	if (Config::IssueInitial) {
	  if (i == 0)
		return Package::create(0, Config::IBSize);
	  i--;
	}

	for (unsigned int s = Config::IBSize; s < 8 * sizeof(Int); s++) {
	  int nbits = ceil(Config::FancyBlockScheme::packagebits(s));
	  assert(nbits >= 8);
	  Int packagesize = 1_u128 << nbits;
	  long npackages = 1_u128 << (s - nbits);
	  assert(npackages >= 0);
	  if (i < npackages)
		return Package::create((1_u128 << s) + i * packagesize, nbits);

	  i -= npackages;
	}

	assert(false);
	__builtin_unreachable();
  }

  __host__ __device__ static auto initial_pair_block(long i) {
	Int packagesize = 1_u128 << Config::SimpleBlockScheme::BSize;
	Int offset = Config::IssueInitial ? 0 : 1_u128 << Config::IBSize;
	return Package::create(static_cast<Int>(i) * packagesize + offset,
						   Config::SimpleBlockScheme::BSize);
  }

  __host__ __device__ static auto initial(long i) {
	if (Config::UseSimpleBlockScheme)
	  return initial_pair_block(i);
	else
	  return initial_pair_fancy(i);
  }

  template<typename Interface>
  __device__ auto run(Interface i, Package p) {
	if (!is_viable(p.lv(), p.bsize()))
	  return i.ret();
	if (!p.splittable(*this))
	  return i.success(p);
	return i.ret(p.split_lower(), p.split_upper());
  }
};

using C = RunConfig;

int
qsum(int N, __uint128_t i) {
  int acc = 0;
  while (i != 0) {
	acc += i % N;
	i /= N;
  }
  return acc;
}

std::pair<uint64_t, uint64_t>
split128(__uint128_t i) {
  return {static_cast<uint64_t>(i & static_cast<uint64_t>(-1l)),
		  static_cast<uint64_t>((i >> 64) & static_cast<uint64_t>(-1l))};
}


template<typename Config>
__host__ double
calc_max(long n = C::NumExtraBlocks) {
  return static_cast<double>(
		  Runner<C>::initial(32l * n + C::NBlocks * C::NThreads).hv());
}

int
towards_zero(int i) {
  if (i > 0)
	return i - 1;
  if (i < 0)
	return i + 1;
  return 0;
}

template<typename F>
void
chk_range(__uint128_t n, unsigned int range_bit, F &&f) {
  if (is_viable(n, range_bit)) {
	if (range_bit == 0) {
	  f(n);
	} else {
	  chk_range(n, range_bit - 1, f);
	  chk_range(n + (ONE << (range_bit - 1)), range_bit - 1, f);
	}
  }
}

template<typename T>
struct AsyncCopyCalc {
  FancyWindow &f;

  std::thread wthread;
  AsyncRB<T, C::AsyncRBSize> rb;

  float ilast;
  float dlast;
  float last;

  int last_issued;
  int startup_penalty;
  int num_found;

  volatile uint64_t taken_pkgs;

  volatile bool halt_wthreads;
  volatile bool start_wthreads;
  volatile __uint128_t last_min;
  volatile __uint128_t last_max;

  FILE *results;

  explicit AsyncCopyCalc(FancyWindow &f):
	  f(f), ilast(0), dlast(0), last(0), last_issued(0), startup_penalty(70),
	  num_found(0), taken_pkgs(0), halt_wthreads(false), start_wthreads(false),
	  last_max(0), results(fopen(C::result_fname, "a")) {
	fprintf(results, "###############################################\n");
	fflush(results);
	wthread = std::thread([&]() { this->run_wthreads(); });
  }

  ~AsyncCopyCalc() { fclose(results); }

  AsyncCopyCalc(const AsyncCopyCalc &) = delete;

  template<typename IFace>
  void adjust_work(IFace iface, uint64_t nnew) {
	float err = 2.0 * (rb.Size / 2.0 - (rb.csize() + nnew)) / rb.Size;

	float delta = err - last;
	last = err;

	float i = ilast + C::PID::I * err;
	i = std::min(std::max(i, -40.f), 80.f);
	float p = C::PID::P * err;
	float d = C::PID::Ddec * dlast + (1 - C::PID::Ddec) * C::PID::D * delta;
	f.ctrl_p.push(p);
	f.ctrl_i.push(i);
	f.ctrl_d.push(d);

	ilast = i;
	dlast = d;

	float out = i + p + d + C::InitialPackageSize;

	int pkgsize = out;
	int frac = std::max((out - pkgsize) * C::NBlocks, 0.f);
	pkgsize = MIN(MAX(pkgsize, 8), 80 - startup_penalty);
	startup_penalty = towards_zero(startup_penalty);
	tsprintf("Ctrl: p %f i %f d %f => %f (%d;%d)\n", p, i, d, out, pkgsize,
			 frac);

	iface.adjust_pkgsize(pkgsize, frac);

	f.pkg_size.push(out - C::InitialPackageSize);
  }

  void emergency_reduce() {
	printf("\033[38;5;208m[Warning]\033[0m Emergency Reduce\n");
	ilast -= C::EmergencyPkgSizeDecrease;
  }

  template<typename GM, typename IFace>
  void operator()(GM gpu_mirror, IFace iface) {
	namespace c = std::chrono;
	auto start = c::high_resolution_clock::now();
	int num = gpu_mirror.num();
	if (num > (1 << C::SuccessQueueSize) / 16)
	  emergency_reduce();
	if (num > (1 << C::SuccessQueueSize) / 4)
	  printf("\033[31;1m[ERROR]\033[0m SuccessQueue wrap possible: %f\n",
			 static_cast<float>(num) / (1 << C::SuccessQueueSize));
	adjust_work(iface, num);
	uint64_t ccsize = rb.csize();
	auto nissued = gpu_mirror.num_issued();
	auto delta = nissued - last_issued;
	auto maxch = calc_max<C>(nissued);
	tsprintf(
			"Copying %d (%.3f%%) elements || RB: %.4f%% (%lu elements) || "
			"Issued: %d = l + %d (%.3f%%) < %.4e || taken %lu min %.4e max %.4e"
			"|| Results %d\n",
			num, percent(num, 1 << C::SuccessQueueSize),
			percent(ccsize, rb.Size), ccsize, nissued, delta,
			percent(nissued, C::NumExtraBlocks), maxch, taken_pkgs,
			static_cast<float>(last_min), static_cast<float>(last_max),
			num_found);
	f.percentage.push(percent(nissued, C::NumExtraBlocks));
	f.delta_issued.push(delta);
	f.received.push(num);
	f.taken.push(taken_pkgs);
	f.rbsize.push(percent(ccsize, rb.Size));
	f.min_rec.push(last_min);
	f.max_rec.push(last_max);
	f.max_checked.push(maxch);

	taken_pkgs = 0;
	last_min = std::numeric_limits<__uint128_t>::max();
	last_max = 0;
	last_issued = nissued;
	f.num_found = num_found;

	rb.spin_push([&](uint64_t i) { return gpu_mirror[i]; }, num);

	f.cpu_time.push(time_delta(start));
	std::this_thread::sleep_until(start + c::milliseconds(C::MinCopyDuration));
  }

  void loop_wthreads(T *buf) {
	uint64_t fetched = rb.try_pop(buf, C::WThreadSize);
	// if (fetched != 0)
	//   printf("%d: Working on %d elements\n", omp_get_thread_num(), fetched);
	__uint128_t min = std::numeric_limits<__uint128_t>::max();
	__uint128_t max = 0;
	__atomic_fetch_add(&taken_pkgs, fetched, __ATOMIC_SEQ_CST);
	for (uint64_t i = 0; i < fetched; i++) {
	  auto success_rec = [&](__uint128_t n) {
		auto decrep = print_128_dec(n);
		printf("\n---------------------------------------------- \n=== "
			   "%s\n---------------------------------------------- \n\n",
			   decrep.c_str());
		fprintf(results, "%s\n", decrep.c_str());
		fflush(results);
		f.last_found = n;
		__atomic_fetch_add(&num_found, 1, __ATOMIC_SEQ_CST);
	  };
	  auto p = buf[i];
	  min = std::min(min, p.lv());
	  max = std::max(max, p.hv());
	  auto upper = p.split_upper();
	  auto lower = p.split_lower();
	  chk_range(lower.lv(), lower.bsize(), success_rec);
	  chk_range(upper.lv(), upper.bsize(), success_rec);
	}
	// tsprintf("Finished package\n");
#pragma omp critical
	{
	  last_min = MIN(last_min, min);
	  last_max = MAX(last_max, max);
	}
  }

  void run_wthreads() {
	while (!start_wthreads)
	  std::this_thread::sleep_for(std::chrono::milliseconds(1));
	// this is a bit of a hack and should prob be done cleaner
	// std::this_thread::sleep_for(50ms);
	tsprintf("Worker threads started\n");
#pragma omp parallel
	{
	  T *buf = (T *) malloc(sizeof(T) * C::WThreadSize);
	  while (!halt_wthreads || (halt_wthreads && rb.csize() != 0)) {
		loop_wthreads(buf);
#pragma omp master
		if (halt_wthreads)
		  tsprintf("Finishing: %.4f%% (%lu elements) remaining\n",
				   percent(rb.csize(), rb.Size), rb.csize());
	  }
	  free(buf);
	}
  }

  void start_calc() { start_wthreads = true; }

  void done() {
	halt_wthreads = true;
	wthread.join();
  };
};

int
main(int argc, char **argv) {
  FancyWindow *f = new FancyWindow();
  AsyncCopyCalc<Runner<C>::Package> async_calc(*f);

  printf("> Searching numbers <= %e\n", calc_max<C>());
  printf("> NThreads = %d NBlocks = %d IBSize=%d IssueInitial= %d\n",
	 C::NThreads, C::NBlocks, C::IBSize, C::IssueInitial);
  printf("> NumExtraBlocks = %d UseSimpleBlockScheme = %d SuccessQueueSize = %d\n",
	 C::NumExtraBlocks, C::UseSimpleBlockScheme, C::SuccessQueueSize);
  printf("> PkgSize Function \"%s\"\n",
		 C::FancyBlockScheme::source_packagebits);
  printf("> WThreadSize = %d MinCopyDuration = %d InitialPackageSize = %d"
	 " EmergendyPkgSizeDecrease = %d\n",
	 C::WThreadSize, C::MinCopyDuration, C::InitialPackageSize,
	 C::EmergencyPkgSizeDecrease);
  printf ("> P = %f I = %f D = %f Ddec = %f\n", C::PID::P, C::PID::I, C::PID::D, C::PID::Ddec);

  if (C::show_gui)
	f->run_async();

  for (int i = 0; i < f->max_checked.RBSize; i++) {
	f->max_checked.getabs(i) =
			Runner<C>::initial(C::NBlocks * C::NThreads).hv();
	f->min_rec.getabs(i) = Runner<C>::initial(C::NBlocks * C::NThreads).lv();
  }

  auto time =
		  WQInterface::Task<Runner<C>::Package, Runner<C>, C>::run(async_calc);

  async_calc.done();

  printf("\n--- Report --------------------\n");
  printf("* Kernel execution time: %2fms\n", time);
  printf("* Searched numbers <= %e\n", calc_max<C>());

  if (C::show_gui)
	f->kill_ui();

  delete f;

  return 0;
}
