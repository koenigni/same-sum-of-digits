#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define ONE         ((__uint128_t) 1)
#define TWO_64      (((__uint128_t) 1) << 64)
#define MUL64(shft) ((uint64_t) ((ONE << shft) % N_BLOCK))

#define CONCAT(x, y)  CONCAT_(x, y)
#define CONCAT_(x, y) x##y

#define PREFIX

#define DEBUG 0

/* Ist das hier universell? */

#define N1 3
#define N2 5

#define BASE       19
#define N_BLOCK    893871739 /* 19 ^7 */
#define N_BLOCK_19 893871739 /* 19 ^7 */
#define BLOCKSZ    7
#define BLOCK_REST 2

#define MAGIC_LOW 0xB9467897F14688B3
#define MAGIC_HI  0xB5DA05E39E4B3D56

/* inv_mod (19^7, 2^128) = 241722307398060865067335033754613090483 */

#include "hilo.inc"

#undef BASE
#undef BLOCKSZ
#undef BLOCK_REST
#undef N_BLOCK
#undef MAGIC_LOW
#undef MAGIC_HI

#define N_BLOCK    410338673 /* 17 ^ 7 */
#define N_BLOCK_17 410338673 /* 17 ^ 7 */
#define BASE       17
#define BLOCKSZ    7
#define BLOCK_REST 2 /* 17^(5*7+2) > 2^128 */

/* inv_mod(17^7,2^128) = 272209719913469092343016337707071495057 */

#define MAGIC_LOW 0x8DDC9A833FECDB91
#define MAGIC_HI  0xCCC9AF60F96F7596

#include "hilo.inc"

#undef BASE
#undef BLOCKSZ
#undef BLOCK_REST
#undef N_BLOCK
#undef MAGIC_LOW
#undef MAGIC_HI

#define BASE       13
#define N_BLOCK    62748517 /* 13 ^ 7 */
#define N_BLOCK_13 62748517 /* 13 ^ 7 */
#define BLOCKSZ    7
#define BLOCK_REST 7 /* 13^(4*7 + 7) > 2^128 */

/* inv_mod(13^7,2^128) = 6004333435185910661695679739661509229 */

#define MAGIC_LOW 0x76679FBAF2E9E26D
#define MAGIC_HI  0x0484647E8AF9B4DB

#include "hilo.inc"

#undef BASE
#undef BLOCKSZ
#undef BLOCK_REST
#undef N_BLOCK
#undef MAGIC_LOW
#undef MAGIC_HI

#define BASE       11
#define BLOCKSZ    8
#define N_BLOCK    214358881 /* 11^8  */
#define N_BLOCK_11 214358881 /* 11^8  */
#define BLOCK_REST 6         /* 11^(4*8 + 6) < 2^128 */

/* inv_mod(11^8,2^128) = 295936467840155828342066850278197758113  */

#define MAGIC_LOW 0x6431A603753508A1
#define MAGIC_HI  0xDEA34A5E885F7915

#include "hilo.inc"

#undef BASE
#undef BLOCKSZ
#undef BLOCK_REST
#undef N_BLOCK
#undef MAGIC_LOW
#undef MAGIC_HI

#define BASE       7
#define BLOCKSZ    10
#define N_BLOCK    282475249
#define N_BLOCK_7  282475249
#define BLOCK_REST 7

/* inv_mod(7^10, 2^128 = 187166794179055117067124501605218219537 */

#define MAGIC_LOW 0x3D36615ABE4B7611
#define MAGIC_HI  0x8CCF036AEF39442D

#include "hilo.inc"

#undef BASE
#undef BLOCKSZ
#undef BLOCK_REST
#undef N_BLOCK
#undef MAGIC_LOW
#undef MAGIC_HI

#define BASE       5
#define BLOCKSZ    12
#define N_BLOCK    244140625
#define N_BLOCK_5  244140625
#define BLOCK_REST 7

/* inv_mod(5^12,2^128) = 235720868483774230372311870550731407025 */

#define MAGIC_LOW 0x3662E0E1CF503EB1
#define MAGIC_HI  0xB156301B10C40B34

#include "hilo.inc"

#undef BASE
#undef BLOCKSZ
#undef BLOCK_REST
#undef N_BLOCK
#undef MAGIC_LOW
#undef MAGIC_HI

#define BASE       3
#define BLOCKSZ    17
#define N_BLOCK    129140163
#define N_BLOCK_3  129140163
#define BLOCK_REST 13

#define MAGIC_LOW 0x2563F4C55A7292EB
#define MAGIC_HI  0xD7C4FE9C06A09457

#include "hilo.inc"

#undef BASE
#undef BLOCKSZ
#undef BLOCK_REST
#undef N_BLOCK
#undef MAGIC_LOW
#undef MAGIC_HI

#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) < (b) ? (a) : (b))

#ifdef __CUDA_ARCH__
#define POP(a) __popcll(a)
#else
#define POP(a) __builtin_popcountl(a)
#endif

__host__ __device__ void
calc_2(__uint128_t from, unsigned int range_bit, unsigned int *lowval,
	   unsigned int *highval) {
  uint64_t hi, lo;
  unsigned int lo_ret, hi_ret;

  hi = from >> 64;
  lo = from;
  lo_ret = POP(hi) + POP(lo);
  hi_ret = lo_ret + range_bit;
  *lowval = lo_ret;
  *highval = hi_ret;
}

__host__ __device__ bool
is_viable(__uint128_t from, unsigned int range_bit) {
  unsigned int minv_ges, maxv_ges, minv, maxv;
  calc_2(from, range_bit, &minv_ges, &maxv_ges);

#if 0
  calc_19 (from, range_bit, &minv, &maxv);
  minv_ges = MAX (minv, minv_ges);
  maxv_ges = MIN (maxv, maxv_ges);
  if (minv_ges > maxv_ges)
    return false;
#endif

  calc_17(from, range_bit, &minv, &maxv);
  minv_ges = MAX(minv, minv_ges);
  maxv_ges = MIN(maxv, maxv_ges);
  if (minv_ges > maxv_ges)
	return false;

  calc_13(from, range_bit, &minv, &maxv);
  minv_ges = MAX(minv, minv_ges);
  maxv_ges = MIN(maxv, maxv_ges);
  if (minv_ges > maxv_ges)
	return false;

  calc_11(from, range_bit, &minv, &maxv);
  minv_ges = MAX(minv, minv_ges);
  maxv_ges = MIN(maxv, maxv_ges);
  if (minv_ges > maxv_ges)
	return false;

  calc_7(from, range_bit, &minv, &maxv);
  minv_ges = MAX(minv, minv_ges);
  maxv_ges = MIN(maxv, maxv_ges);
  if (minv_ges > maxv_ges)
	return false;

#ifndef __CUDA_ARCH__
  calc_5(from, range_bit, &minv, &maxv);
  minv_ges = MAX(minv, minv_ges);
  maxv_ges = MIN(maxv, maxv_ges);
  if (minv_ges > maxv_ges)
	return false;

  calc_3(from, range_bit, &minv, &maxv);
  minv_ges = MAX(minv, minv_ges);
  maxv_ges = MIN(maxv, maxv_ges);

#endif
  return minv_ges <= maxv_ges;
}

