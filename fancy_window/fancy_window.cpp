// Dear ImGui: standalone example application for GLFW + OpenGL 3, using
// programmable pipeline (GLFW is a cross-platform general purpose library for
// handling windows, inputs, OpenGL/Vulkan/Metal graphics context creation,
// etc.) If you are new to Dear ImGui, read documentation from the docs/ folder
// + read the top of imgui.cpp. Read online:
// https://github.com/ocornut/imgui/tree/master/docs

#include "fancy_window.hpp"

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include "implot.h"
#include <algorithm>
#include <math.h>
#include <stdio.h>
#include <string>
#define GL_SILENCE_DEPRECATION
#if defined(IMGUI_IMPL_OPENGL_ES2)
#include <GLES2/gl2.h>
#endif
#include <GLFW/glfw3.h>// Will drag system OpenGL headers

static void
glfw_error_callback(int error, const char *description) {
  fprintf(stderr, "GLFW Error %d: %s\n", error, description);
}

void
run_gui(FancyWindow &f) {
  glfwSetErrorCallback(glfw_error_callback);
  if (!glfwInit())
	return;

  // GL 3.0 + GLSL 130
  const char *glsl_version = "#version 130";
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
  // glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+
  // only glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // 3.0+ only

  // Create window with graphics context
  GLFWwindow *window =
		  glfwCreateWindow(1280, 720, "Readout Screen", nullptr, nullptr);
  if (window == nullptr)
	return;
  glfwMakeContextCurrent(window);
  glfwSwapInterval(1);// Enable vsync

  // Setup Dear ImGui context
  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImPlot::CreateContext();

  ImGuiIO &io = ImGui::GetIO();
  (void) io;
  io.ConfigFlags |=
		  ImGuiConfigFlags_NavEnableKeyboard;// Enable Keyboard Controls
  io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;// Enable Gamepad Controls

  // Setup Dear ImGui style
  ImGui::StyleColorsDark();
  // ImGui::StyleColorsLight();

  // Setup Platform/Renderer backends
  ImGui_ImplGlfw_InitForOpenGL(window, true);
  ImGui_ImplOpenGL3_Init(glsl_version);

  ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

  while (!glfwWindowShouldClose(window)
		 && !__atomic_load_n(&f.done, __ATOMIC_SEQ_CST)) {
	glfwPollEvents();

	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();

	const ImGuiViewport *viewport = ImGui::GetMainViewport();
	ImGui::SetNextWindowPos(viewport->Pos);
	ImGui::SetNextWindowSize(viewport->Size);

	// 2. Show a simple window that we create ourselves. We use a Begin/End pair
	// to create a named window.
	{
	  static ImGuiWindowFlags flags = ImGuiWindowFlags_NoDecoration
									  | ImGuiWindowFlags_NoMove
									  | ImGuiWindowFlags_NoSavedSettings;
	  bool p = true;
	  ImGui::Begin("Calculation Monitor", &p, flags);

	  auto last = f.last_found;
	  ImGui::Text("Last found: %#lx%lx", (uint64_t) (last >> 64),
				  (uint64_t) last);
	  ImGui::Text("Number of results: %d", f.num_found);

	  if (ImPlot::BeginPlot("Progress")) {
		ImPlot::SetupAxisLimits(ImAxis_Y1, 0, 100);
		ImPlot::SetupAxisLimits(ImAxis_X1, 0, f.percentage.RBSize);
		ImPlot::PlotLine("Percentage", (float *) (f.percentage.get_ptr()),
						 f.percentage.RBSize);
		ImPlot::EndPlot();
	  }
	  if (ImPlot::BeginPlot("Blocks")) {
		volatile float *p = f.delta_issued.get_ptr();
		ImPlot::SetupAxes(nullptr, "#Blocks", ImPlotAxisFlags_AutoFit,
						  ImPlotAxisFlags_AutoFit);
		ImPlot::PlotLine("Delta Issued", (float *) p, f.delta_issued.RBSize);
		ImPlot::EndPlot();
	  }
	  if (ImPlot::BeginPlot("Ring Buffer")) {
		volatile float *p = f.received.get_ptr();
		volatile float *p2 = f.taken.get_ptr();
		volatile float *p3 = f.rbsize.get_ptr();

		ImPlot::SetupAxes(nullptr, "#Elements Rec/Taken",
						  ImPlotAxisFlags_AutoFit, ImPlotAxisFlags_AutoFit);
		ImPlot::SetupAxis(ImAxis_Y2, "RB Size [%]",
						  ImPlotAxisFlags_AutoFit | ImPlotAxisFlags_Opposite);
		ImPlot::SetAxes(ImAxis_X1, ImAxis_Y1);
		ImPlot::PlotLine("Received", (float *) p, f.received.RBSize);
		ImPlot::PlotLine("Taken", (float *) p2, f.received.RBSize);

		ImPlot::SetAxes(ImAxis_X1, ImAxis_Y2);
		ImPlot::PlotLine("Ring Buffer Size", (float *) p3, f.rbsize.RBSize);

		ImPlot::EndPlot();
	  }
	  if (ImPlot::BeginPlot("Numbers")) {
		volatile float *p = f.max_checked.get_ptr();
		volatile float *p2 = f.min_rec.get_ptr();
		volatile float *p3 = f.max_rec.get_ptr();
		ImPlot::SetupAxes(nullptr, "Num", ImPlotAxisFlags_AutoFit,
						  ImPlotAxisFlags_AutoFit);
		ImPlot::SetupAxisScale(ImAxis_Y1, ImPlotScale_Log10);
		ImPlot::PlotLine("Max Issued", (float *) p, f.max_checked.RBSize);
		ImPlot::PlotLine("Min Rec", (float *) p2, f.min_rec.RBSize);
		ImPlot::PlotLine("Max Rec", (float *) p3, f.max_rec.RBSize);
		ImPlot::EndPlot();
	  }
	  if (ImPlot::BeginPlot("Copy time")) {
		volatile float *p = f.cpu_time.get_ptr();
		ImPlot::SetupAxes(nullptr, "Time [ms]", ImPlotAxisFlags_AutoFit,
						  ImPlotAxisFlags_AutoFit);
		ImPlot::PlotLine("Copy time", (float *) p, f.cpu_time.RBSize);
		ImPlot::EndPlot();
	  }
	  if (ImPlot::BeginPlot("Controller information")) {
		volatile float *p = f.pkg_size.get_ptr();
		volatile float *p2 = f.ctrl_p.get_ptr();
		volatile float *p3 = f.ctrl_i.get_ptr();
		volatile float *p4 = f.ctrl_d.get_ptr();

		ImPlot::SetupAxes(nullptr, "Bits", ImPlotAxisFlags_AutoFit,
						  ImPlotAxisFlags_AutoFit);
		ImPlot::PlotLine("Package Size Delta", (float *) p, f.pkg_size.RBSize);
		ImPlot::PlotLine("P", (float *) p2, f.pkg_size.RBSize);
		ImPlot::PlotLine("I", (float *) p3, f.pkg_size.RBSize);
		ImPlot::PlotLine("D", (float *) p4, f.pkg_size.RBSize);
		ImPlot::EndPlot();
	  }

	  ImGui::End();
	}

	// Rendering
	ImGui::Render();
	int display_w, display_h;
	glfwGetFramebufferSize(window, &display_w, &display_h);
	glViewport(0, 0, display_w, display_h);
	glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w,
				 clear_color.z * clear_color.w, clear_color.w);
	glClear(GL_COLOR_BUFFER_BIT);
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

	glfwSwapBuffers(window);
  }
#ifdef __EMSCRIPTEN__
  EMSCRIPTEN_MAINLOOP_END;
#endif

  // Cleanup
  ImGui_ImplOpenGL3_Shutdown();
  ImGui_ImplGlfw_Shutdown();
  ImPlot::DestroyContext();
  ImGui::DestroyContext();

  glfwDestroyWindow(window);
  glfwTerminate();
}

FancyWindow::FancyWindow() noexcept:
	percentage(), delta_issued(), cpu_time(), received(), max_checked(),
	min_rec(), rbsize(), last_found(0), num_found(0), t(), done(false) {}

void
FancyWindow::run_async() {
  for (int i = 0; i < this->max_checked.RBSize; i++)
	this->max_checked.get_ptr()[i] = 1.f;
  this->t = std::thread([&]() { run_gui(*this); });
}


void
FancyWindow::kill_ui() {
  __atomic_store_n(&this->done, true, __ATOMIC_SEQ_CST);
  this->t.join();
}

