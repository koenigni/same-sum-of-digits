#pragma once

#include <cstdlib>
#include <thread>
#include <vector>

template<typename T, int Size>
struct RingBufferSRSW {
  volatile T *p;
  volatile unsigned int head;

  RingBufferSRSW(): p((T *) calloc(Size, sizeof(T))), head(0) {}
  ~RingBufferSRSW() { free((T *) p); }

  void push(T t) {
	unsigned int s = head;
	head = (s + 1) & (Size - 1);
	p[s] = t;
  }

  volatile T &operator[](int i) { return p[(head + i - 1) & (Size - 1)]; }
  volatile T &getabs(int i) { return p[i]; }
  volatile T *get_ptr() { return p; }

  static constexpr int RBSize = Size;
};

struct FancyWindow {
  static constexpr unsigned BufSize = 0x400;

  RingBufferSRSW<float, BufSize> percentage;
  RingBufferSRSW<float, BufSize> delta_issued;
  RingBufferSRSW<float, BufSize> cpu_time;
  RingBufferSRSW<float, BufSize> received;
  RingBufferSRSW<float, BufSize> taken;
  RingBufferSRSW<float, BufSize> max_checked;
  RingBufferSRSW<float, BufSize> min_rec;
  RingBufferSRSW<float, BufSize> max_rec;
  RingBufferSRSW<float, BufSize> pkg_size;
  RingBufferSRSW<float, BufSize> ctrl_p;
  RingBufferSRSW<float, BufSize> ctrl_i;
  RingBufferSRSW<float, BufSize> ctrl_d;
  RingBufferSRSW<float, BufSize> rbsize;

  volatile __uint128_t last_found;
  volatile int num_found;

  std::thread t;
  volatile bool done;

  FancyWindow() noexcept;

  FancyWindow(const FancyWindow &) = delete;

  void run_async();
  void kill_ui();
};

