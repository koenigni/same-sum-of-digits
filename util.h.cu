#pragma once

#include <cstdint>
#include <mutex>
#include <stdio.h>
#include <string>

#include <cooperative_groups.h>
#include <thread>

#define FUNC_SOURCE(mod, name, body, ...) \
  static constexpr char source_##name[] = #body; \
  mod auto name(__VA_ARGS__) { \
	body; \
  }

#define CUDA_CHECK(ans) \
  { gpuAssert((ans), __FILE__, __LINE__); }
inline void
gpuAssert(cudaError_t code, const char *file, int line, bool abort = true) {
  if (code != cudaSuccess) {
	fprintf(stderr, "GPUassert: %s @%s:%d\n", cudaGetErrorString(code), file,
			line);
	if (abort)
	  exit(code);
  }
}

template<typename R, typename T>
static inline __device__ __host__ R
convert(T a) {
  union Converter {
	T a;
	R r;
  };

  return Converter{a}.r;
}

__host__ __device__ constexpr __uint128_t
operator""_u128(const char *c) {
  __uint128_t res = 0;
  if (c[0] != '\0' && c[1] != '\0' && c[0] == '0' && c[1] == 'x') {
	c += 2;
	for (; *c != '\0'; c++) {
	  res *= 16;
	  if (*c >= 'a' && *c <= 'f')
		res += *c - 'a' + 10;
	  else if (*c >= 'A' && *c <= 'F')
		res += *c - 'A' + 10;
	  else if (*c >= '0' && *c <= '9')
		res += *c - '0';
	}
  } else {
	for (; *c != '\0'; c++) {
	  res *= 10;
	  if (*c >= '0' && *c <= '9')
		res += *c - '0';
	}
  }
  return res;
}

#define T18 (1000000ul * 1000000ul * 1000000ul)

__host__ std::string
print_128_dec(__uint128_t a) {
  uint64_t a0, a1, a2;
  char buf[40];
  if (a == 0) {
	sprintf(buf, "%38s", "0");
	return std::string(buf);
  }
  a0 = a % T18;
  a = a / T18;
  a1 = a % T18;
  a2 = a / T18;
  sprintf(buf, "%2.2lu%18.18lu%18.18lu", a2, a1, a0);
  for (char *p = buf; *p == '0'; p++)
	*p = ' ';
  return std::string(buf);
}

template<typename T, int Size>
struct Padded {
  union {
	T t;
	char padding[Size];
  } v;

  operator T() { return v.t; }
  T &operator()() { return v.t; }
};

template<typename T, int Bits>
struct AsyncRB {
  static constexpr uint64_t Size = 1ul << Bits;

  struct Data {
	volatile T ts[Size];
	volatile int begin, end;
	std::mutex m;
  };

  AsyncRB(): d(new Data) {}
  ~AsyncRB() { delete d; }
  AsyncRB(const AsyncRB &) = delete;

  Data *d;

  uint64_t endv() { return d->end & (Size - 1); }
  uint64_t beginv() { return d->begin & (Size - 1); }
  uint64_t csize() { return (d->end - d->begin) & (Size - 1); }

  T &erel(uint64_t i) { return *(T *) &d->ts[(d->end + i) & (Size - 1)]; }
  T &brel(uint64_t i) { return *(T *) &d->ts[(d->begin + i) & (Size - 1)]; }

  template<typename G>
  bool push_gen(G &&g, uint64_t n) {
	std::lock_guard<std::mutex> lg{d->m};
	int cs = csize();
	if (cs + n > Size)
	  return false;
	for (uint64_t i = 0; i < n; i++)
	  erel(i) = g(i);
	d->end += n;
	return true;
  }

  template<typename G>
  void spin_push(G &&g, uint64_t n) {
	for (;;) {
	  if (push_gen(g, n))
		return;
	  while (csize() + n > Size)
		std::this_thread::sleep_for(std::chrono::nanoseconds(10000));
	}
  }

  int try_pop(T *p, uint64_t n) {
	std::lock_guard<std::mutex> lg{d->m};
	uint64_t nact = std::min(csize(), n);
	for (uint64_t i = 0; i < nact; i++)
	  p[i] = brel(i);
	d->begin += nact;
	return nact;
  }
};

template<typename T, int Bits>
struct AsyncStack {
  static constexpr uint64_t Size = 1ul << Bits;

  struct Data {
	volatile T ts[Size];
	volatile uint64_t head;
	std::mutex m;
  };

  AsyncStack(): d(new Data) { d->head = 0; }
  ~AsyncStack() { delete d; }
  AsyncStack(const AsyncStack &) = delete;

  Data *d;

  T &operator[](uint64_t i) { return *(T *) &d->ts[i]; }

  uint64_t csize() { return d->head; }

  template<typename G>
  bool push_gen(G &&g, uint64_t n) {
	std::lock_guard<std::mutex> lg{d->m};
	uint64_t cs = d->head;
	if (cs + n > Size)
	  return false;
	for (uint64_t i = 0; i < n; i++)
	  (*this)[cs + i] = g(i);
	d->head += n;
	return true;
  }

  template<typename G>
  void spin_push(G &&g, uint64_t n) {
	for (;;) {
	  if (push_gen(g, n))
		return;
	  while (csize() + n > Size)
		std::this_thread::sleep_for(std::chrono::nanoseconds(10000));
	}
  }

  int try_pop(T *p, uint64_t n) {
	std::lock_guard<std::mutex> lg{d->m};
	uint64_t chead = d->head;
	uint64_t nact = std::min(chead, n);
	for (uint64_t i = 0; i < nact; i++)
	  p[i] = (*this)[chead - nact + i];
	d->head -= nact;
	return nact;
  }
};

template<typename TP>
auto
time_delta(TP start) {
  return std::chrono::duration_cast<std::chrono::milliseconds>(
				 std::chrono::high_resolution_clock::now() - start)
		  .count();
}

static auto prog_start = std::chrono::high_resolution_clock::now();// XXX

#define tsprintf(fmt, ...) \
  do { \
	long _ms = time_delta(prog_start); \
	long _s = _ms / 1000; \
	_ms = _ms % 1000; \
	printf("[%ld.%03ld] " fmt, _s, _ms, ##__VA_ARGS__); \
  } while (0)

template<typename T, typename S>
float
percent(T a, S b) {
  return 100.0f * static_cast<float>(a) / b;
}

#ifdef DEBUG_MODE

#define DPRINT(s, ...) \
  printf("T%-3d(%d):\t" s "\n", threadIdx.x, blockIdx.x, ##__VA_ARGS__)

#define CONCAT_(x, y) x##y
#define CONCAT(x, y)  CONCAT_(x, y)

#define DPRINT_AT_EXIT(s, ...) \
  auto CONCAT(dprinthelper, __COUNTER__) = \
		  make_dprinthelper([&]() { DPRINT(s, ##__VA_ARGS__); });

#define DPRINT_ONCE(s, ...) \
  do { \
	cooperative_groups::invoke_one(cooperative_groups::coalesced_threads(), \
								   [&]() { DPRINT(s, ##__VA_ARGS__); }); \
  } while (0)

template<typename F>
struct DPrintHelper {
  F f;
  __device__ DPrintHelper(F &f): f(f){};

  __device__ ~DPrintHelper() { f(); }
};

template<typename F>
__device__ auto
make_dprinthelper(F f) {
  return DPrintHelper<F>{f};
}
#else

#ifdef __CUDA_ARCH__
#warning "Compiling in Release mode"
#endif

#define DPRINT(...)
#define DPRINT_AT_EXIT(...)
#define DPRINT_ONCE(...)

#endif

