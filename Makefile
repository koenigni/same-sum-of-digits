SM=sm_75
NREGS=64

HOSTCFLAGS=-Wall -march=native -Wno-unknown-pragmas -fopenmp 
DEVICECFLAGS=-O3 -g -G -dopt on -maxrregcount $(NREGS) --resource-usage -arch=$(SM) -restrict -noeh
# --compiler-bindir /usr/bin
CC=nvcc

LIBS = -lGL `pkg-config --static --libs glfw3`

HOSTPREFIX=-Xcompiler\

NVCC_HOSTCFLAGS=$(addprefix $(HOSTPREFIX) , $(HOSTCFLAGS))

.PHONY: all fancy_window run

all: a.out

a.out: main.cu work_queue.h.cu util.h.cu hilo.inc hilo.h.cu Makefile fancy_window
	$(CC) $(NVCC_HOSTCFLAGS) $(DEVICECFLAGS) $< fancy_window/fancy_window.a $(LIBS)

run: a.out
	stdbuf --output=L ./a.out | tee res.log

fancy_window:
	$(MAKE) -C fancy_window/

clean:
	$(MAKE) -C fancy_window/ clean

distclean: clean
	$(RM) a.out
	$(RM) -r .cache

bear:
	bear -- $(MAKE) all

commit:
	git add --all
	git commit -am '*'
	git push
